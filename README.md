# libmks

libmks is container with mks binary and libmks compiled

It builds gnome libmks project, and store only binaries
https://gitlab.gnome.org/GNOME/libmks

**How to build and run this image**:

``` sh
# podman build -f Containerfile -t localhost/libmks:latest .
# podman run --name libmks  -it localhost/libmks bash -c /tools/mks
```

Full demo usage could be find in that bugzilla
https://bugzilla.redhat.com/show_bug.cgi?id=2207940#c12

